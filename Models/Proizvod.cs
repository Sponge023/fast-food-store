﻿using System.ComponentModel.DataAnnotations;

namespace EPromet.Models
{
    public class Proizvod
    {
        public Proizvod()
        {
        }

        public Proizvod(int id, string naziv, int cena, string sastojci)
        {
            Id = id;
            Naziv = naziv;
            Cena = cena;
            Sastojci = sastojci;
        }

        [Required]
        [Key]
        public int Id { get; set; }


        [Required]
        [Display(Name = "Naziv proizvoda")]
        public string Naziv { get; set; }


        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Cena")]
        public double Cena { get; set; }


        [Display(Name = "Sastojci")]
        [Required]
        public string Sastojci { get; set; }
    }
}
