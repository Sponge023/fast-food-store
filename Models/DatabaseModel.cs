﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using System.Data;
using System.Xml;
using System.Xml.Linq;

namespace EPromet.Models
{
    public class DatabaseModel
    {
        #region Korisnici

        public static List<Korisnik> UcitajKorisnike(IWebHostEnvironment putanja)
        {
            List<Korisnik> korisnici = new List<Korisnik>();

            DataSet ds = new DataSet();

            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;

            string path = "";
            path = Path.Combine(contentRootPath, "Database", "Korisnici.xml");

            ds.ReadXml(path);
            DataView dvPrograms;
            dvPrograms = ds.Tables[0].DefaultView;
            dvPrograms.Sort = "Id";
            foreach (DataRowView dr in dvPrograms)
            {
                Korisnik k = new Korisnik();
                k.Id = Convert.ToInt32(dr[0]);
                k.KorisnickoIme = Convert.ToString(dr[1]);
                k.Email = Convert.ToString(dr[2]);
                k.Lozinka = Convert.ToString(dr[3]);
                k.Ime = Convert.ToString(dr[4]);
                k.Prezime = Convert.ToString(dr[5]);
                k.DatumRodjenja = Convert.ToDateTime(dr[6]);
                k.Adresa = Convert.ToString(dr[7]);
                Enum.TryParse(dr[8].ToString(), out Korisnik.EUloga rola);
                k.Uloga = rola;
                k.AvatarPath = Convert.ToString(dr[9]);
                k.Verifikovan = Convert.ToBoolean(dr[10]);

                korisnici.Add(k);
            }

                return korisnici;
            
        }

        public static bool SacuvajKorisnika(Korisnik k, IWebHostEnvironment putanja)
        {
            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;
            string pathSlike = "";
            string folder="";
            List<Korisnik> korisnici = UcitajKorisnike(putanja); // OVDE SE UCITAVA


            k.Id = new Random().Next(1, 9999);
            foreach (Korisnik kor in korisnici)
            {
                while (k.Id == kor.Id)
                {
                    k.Id++;
                }
            }

            if (k.Avatar != null)
            {
                folder = "Database/Images/";
                folder += $"{k.Id}-{k.KorisnickoIme}.png";
                

                pathSlike = Path.Combine(contentRootPath, folder);
                FileStream fs = new FileStream(pathSlike, FileMode.Create);
                k.Avatar.CopyToAsync(fs);
                fs.Close();
            }
            else
            {
                folder = "Database/Images/noavatar.png";                
            }



            string path = "";

            path = Path.Combine(contentRootPath, "Database", "Korisnici.xml");

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.Load(path);
            


            XDocument xmlDoc = XDocument.Load(path);
            xmlDoc.Element("Korisnici").Add(
                new XElement("Korisnik",
                new XElement("Id", k.Id),
                new XElement("KorisnickoIme", k.KorisnickoIme),
                new XElement("Email", k.Email),
                new XElement("Lozinka", k.Lozinka),
                new XElement("Ime", k.Ime),
                new XElement("Prezime", k.Prezime),
                new XElement("DatumRodjenja", k.DatumRodjenja),
                new XElement("Adresa", k.Adresa),
                new XElement("Uloga", k.Uloga),
                new XElement("AvatarPath", folder),
                new XElement("Verifikovan", k.Verifikovan)));


            xmlDoc.Save(path);

            return true;
        }

        public static void UpdateKorisnika(Korisnik k, IWebHostEnvironment putanja)
        {
            List<Korisnik> korisnici = UcitajKorisnike(putanja); // OVDE SE UCITAVA
            Korisnik korisnik = new Korisnik();

            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;

            string path = "";
            path = Path.Combine(contentRootPath, "Database", "Korisnici.xml");

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.Load(path);


            foreach (Korisnik kor in korisnici)
            {
                if(k.Id == kor.Id)
                {
                    korisnik = k;
                }
            }

            string folder = "";
            string pathSlike = "";
            if (k.Avatar != null)
            {
                folder = "Database/Images/";
                folder += $"{k.Id}-{k.KorisnickoIme}.png";


                pathSlike = Path.Combine(contentRootPath, folder);

                FileStream fs = new FileStream(pathSlike, FileMode.Create);
                k.Avatar.CopyToAsync(fs);
                fs.Close();
            }
            else
            {
                folder = "Database/Images/noavatar.png";
            }

            XDocument xmlDoc = XDocument.Load(path);

            var items = (from item in xmlDoc.Descendants("Korisnik") select item).ToList();
            XElement selected = items.Where(p => p.Element("Id").Value == k.Id.ToString()).FirstOrDefault();
            selected.Remove();


            xmlDoc.Save(path);
            xmlDoc.Element("Korisnici").Add(
                new XElement("Korisnik",
                new XElement("Id", korisnik.Id),
                new XElement("KorisnickoIme", korisnik.KorisnickoIme),
                new XElement("Email", korisnik.Email),
                new XElement("Lozinka", korisnik.Lozinka),
                new XElement("Ime", korisnik.Ime),
                new XElement("Prezime", korisnik.Prezime),
                new XElement("DatumRodjenja", korisnik.DatumRodjenja),
                new XElement("Adresa", korisnik.Adresa),
                new XElement("Uloga", korisnik.Uloga),
                new XElement("AvatarPath", folder),
                new XElement("Verifikovan", korisnik.Verifikovan)));
            xmlDoc.Save(path);


        }

        #endregion

        #region Proizvodi

        public static List<Proizvod> UcitajProizvode(IWebHostEnvironment putanja)
        {
            List<Proizvod> proizvodi = new List<Proizvod>();

            DataSet ds = new DataSet();

            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;

            string path = "";
            path = Path.Combine(contentRootPath, "Database", "Proizvodi.xml");

            ds.ReadXml(path);
            DataView dvPrograms;
            dvPrograms = ds.Tables[0].DefaultView;
            dvPrograms.Sort = "Id";
            foreach (DataRowView dr in dvPrograms)
            {
                Proizvod p = new Proizvod();
                p.Id = Convert.ToInt32(dr[0]);
                p.Naziv = Convert.ToString(dr[1]);
                p.Cena = Convert.ToInt32(dr[2]);
                p.Sastojci = Convert.ToString(dr[3]);

                proizvodi.Add(p);
            }

            return proizvodi;

        }


        public static bool SacuvajProizvod(Proizvod p, IWebHostEnvironment putanja)
        {
            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;
            List<Proizvod> proizvodi = UcitajProizvode(putanja); // OVDE SE UCITAVA


            p.Id = new Random().Next(1, 9999);
            foreach (Proizvod pro in proizvodi)
            {
                while (p.Id == pro.Id)
                {
                    p.Id++;
                }
            }



            string path = "";

            path = Path.Combine(contentRootPath, "Database", "Proizvodi.xml");

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.Load(path);


            XDocument xmlDoc = XDocument.Load(path);
            xmlDoc.Element("Proizvodi").Add(
                new XElement("Proizvod",
                new XElement("Id", p.Id),
                new XElement("Naziv", p.Naziv),
                new XElement("Cena", p.Cena),
                new XElement("Sastojci", p.Sastojci)));


            xmlDoc.Save(path);

            return true;
        }

        public static bool ObrisiProizvod(Proizvod p, IWebHostEnvironment putanja)
        {
            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;
            string path = "";
            path = Path.Combine(contentRootPath, "Database", "Proizvodi.xml");


            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.Load(path);


            XDocument xmlDoc = XDocument.Load(path);


            var items = (from item in xmlDoc.Descendants("Proizvod") select item).ToList();
            XElement selected = items.Where(pr => pr.Element("Id").Value == p.Id.ToString()).FirstOrDefault();
            selected.Remove();



            xmlDoc.Save(path);

            return true;
        }

        #endregion

        #region Porudzbine

        public static List<Porudzbina> UcitajPorudzbine(IWebHostEnvironment putanja)
        {
            List<Porudzbina> porudzbine = new List<Porudzbina>();
            List<Proizvod> proizvodi = DatabaseModel.UcitajProizvode(putanja);

            DataSet ds = new DataSet();

            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;

            string path = "";
            path = Path.Combine(contentRootPath, "Database", "Porudzbine.xml");

            ds.ReadXml(path);
            DataView dvPrograms;
            dvPrograms = ds.Tables[0].DefaultView;
            //dvPrograms.Sort = "Id";
            foreach (DataRowView dr in dvPrograms)                                                              
            {                                                                                                   
                List<Proizvod> pro = new List<Proizvod>();

                if (dr[3] != String.Empty)
                {
                    string[] idevi = dr[3].ToString().Split('|');

                    for (int i = 0; i < idevi.Length-1; i++)
                    {
                        foreach (Proizvod item in proizvodi)
                        {
                            if (Convert.ToInt32(idevi[i]) == item.Id)
                                pro.Add(item);
                        }
                    }
                }                

                Porudzbina p = new Porudzbina();
                p.Id = Convert.ToInt32(dr[0]);
                p.IdKorisnika = Convert.ToInt32(dr[1]);
                p.IdDostavljaca = Convert.ToInt32(dr[2]);
                p.ListaProizvoda = pro;
                p.Adresa = Convert.ToString(dr[4]);
                p.Komentar = Convert.ToString(dr[5]);
                p.Cena = Convert.ToInt32(dr[6]);
                Enum.TryParse(dr[7].ToString(), out Porudzbina.EStatus rola);
                p.Status = rola;


                porudzbine.Add(p);
            }

            return porudzbine;

        }

        public static int SacuvajPorudzbinu(Porudzbina p, IWebHostEnvironment putanja)
        {
            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;
            List<Porudzbina> porudzbine = UcitajPorudzbine(putanja); // OVDE SE UCITAVA

            p.Id = new Random().Next(1, 9999);

            foreach (Porudzbina por in porudzbine)
            {
                while (p.Id == por.Id)
                {
                    p.Id++;
                }
            }

            string path = "";

            path = Path.Combine(contentRootPath, "Database", "Porudzbine.xml");

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.Load(path);

            string ideviProizvoda = "";
            foreach (Proizvod pro in p.ListaProizvoda)
            {
                ideviProizvoda += $"{pro.Id}|";
            }

            XDocument xmlDoc = XDocument.Load(path);
            xmlDoc.Element("Porudzbine").Add(
                new XElement("Porudzbina",
                new XElement("Id", p.Id),
                new XElement("IdKorisnika", p.IdKorisnika),
                new XElement("Dostavljac", p.IdDostavljaca),
                new XElement("Proizvodi", ideviProizvoda),
                new XElement("Adresa", p.Adresa),
                new XElement("Komentar", p.Komentar),
                new XElement("Cena", p.Cena),
                new XElement("Status", p.Status)));


            xmlDoc.Save(path);

            return p.Id;
        }

        public static int UpdatePorudzbine(Porudzbina p, IWebHostEnvironment putanja)
        {
            string webRootPath = putanja.WebRootPath;
            string contentRootPath = putanja.ContentRootPath;
            List<Porudzbina> porudzbine = UcitajPorudzbine(putanja); // OVDE SE UCITAVA



            string path = "";

            path = Path.Combine(contentRootPath, "Database", "Porudzbine.xml");

            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.Load(path);

            string ideviProizvoda = "";
            foreach (Proizvod pro in p.ListaProizvoda)
            {
                ideviProizvoda += $"{pro.Id}|";
            }

            XDocument xmlDoc = XDocument.Load(path);

            var items = (from item in xmlDoc.Descendants("Porudzbina") select item).ToList();
            XElement selected = items.Where(por => por.Element("Id").Value == p.Id.ToString()).FirstOrDefault();
            selected.Remove();


            xmlDoc.Save(path);

            xmlDoc = XDocument.Load(path);
            xmlDoc.Element("Porudzbine").Add(
                new XElement("Porudzbina",
                new XElement("Id", p.Id),
                new XElement("IdKorisnika", p.IdKorisnika),
                new XElement("Dostavljac", p.IdDostavljaca),
                new XElement("Proizvodi", ideviProizvoda),
                new XElement("Adresa", p.Adresa),
                new XElement("Komentar", p.Komentar),
                new XElement("Cena", p.Cena),
                new XElement("Status", p.Status)));


            xmlDoc.Save(path);

            return p.Id;
        }

        #endregion

    }
}
