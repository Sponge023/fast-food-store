﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EPromet.Models
{
    public class Korisnik
    {
        public Korisnik()
        {
        }

        public Korisnik(int id, string korisnickoIme, string email, string lozinka, string ime, string prezime, DateTime datumRodjenja, string adresa, EUloga? uloga, IFormFile? avatar, string avatarPath, bool? verifikovan)
        {
            Id = id;
            KorisnickoIme = korisnickoIme;
            Email = email;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            DatumRodjenja = datumRodjenja;
            Adresa = adresa;
            Uloga = uloga;
            Avatar = avatar;
            AvatarPath = avatarPath;
            Verifikovan = verifikovan;
        }

        [Required]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Korisnicko ime")]
        [Required]
        public string KorisnickoIme { get; set; }


        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }


        [Display(Name = "Lozinka")]
        [DataType(DataType.Password)]
        [Required]
        public string Lozinka { get; set; }

        [Compare("Lozinka", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [Display(Name = "Potvrdi lozinku")]
        [DataType(DataType.Password)]
        [Required]
        public string PotvrdiLozinku { get; set; }


        [Display(Name = "Ime")]
        [Required]
        public string Ime { get; set; }


        [Display(Name = "Prezime")]
        [Required]
        public string Prezime { get; set; }


        [Display(Name = "Datum rodjenja")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime DatumRodjenja { get; set; }


        [Display(Name = "Adresa")]
        [Required]
        public string Adresa { get; set; }


        [Display(Name = "Uloga")]
        public EUloga? Uloga { get; set; } = EUloga.GOST;


        [NotMapped]
        [Display(Name = "Avatar")]
        public IFormFile? Avatar { get; set; }

        public string AvatarPath { get; set; }

        [Display(Name = "Verifikovan")]
        public bool? Verifikovan { get; set; } = false;

        public enum EUloga
        {
            ADMINISTRATOR,
            KORISNIK,
            GOST,
            DOSTAVLJAC
        }

    }
}
