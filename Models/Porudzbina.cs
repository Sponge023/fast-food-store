﻿using System.ComponentModel.DataAnnotations;

namespace EPromet.Models
{
    public class Porudzbina
    {

        [Required]
        [Key]
        [Display(Name ="ID Porudzbine")]
        public int Id { get; set; }
        
        [Required]
        public int IdKorisnika { get; set; }


        public int? IdDostavljaca { get; set; } = 0;

        public List<Proizvod>? ListaProizvoda { get; set; } = new List<Proizvod>();

        [Required]
        public string Adresa { get; set; }


        public string? Komentar { get; set; } = "";

        [DataType(DataType.Currency)]
        public double Cena { get; set; }

        public EStatus Status { get; set; } = EStatus.NEPOTVRDJENA;

        public enum EStatus
        {
            IZVRSENA,
            NEIZVRSENA,
            NA_CEKANJU,
            NEPOTVRDJENA
        }

    }
}
