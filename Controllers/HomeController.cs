﻿using EPromet.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;

namespace EPromet.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IWebHostEnvironment putanja;
        private IEnumerable<Proizvod> proizvodi;

        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment hostEnvironment)
        {
            _logger = logger;
            putanja = hostEnvironment;
        }

        public IActionResult Index()
        {
            if(Global.User == null)
            {
                return RedirectToAction("Login", "Autentifikacija");
            }
            proizvodi = DatabaseModel.UcitajProizvode(putanja);
            return View(proizvodi);
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}