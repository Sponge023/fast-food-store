﻿using EPromet.Models;
using Microsoft.AspNetCore.Mvc;

namespace EPromet.Controllers
{
    public class PorudzbineController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IWebHostEnvironment putanja;
        private IEnumerable<Porudzbina> porudzbine;


        public PorudzbineController(ILogger<HomeController> logger, IWebHostEnvironment hostEnvironment)
        {
            _logger = logger;
            putanja = hostEnvironment;
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult NovePorudzbine()
        {
            porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
            List<Porudzbina> porudzbineTemp = new List<Porudzbina>();
            foreach (Porudzbina por in porudzbine)
            {
                if (por.Status == Porudzbina.EStatus.NA_CEKANJU)
                {
                    porudzbineTemp.Add(por);
                }

            }


            return View(porudzbineTemp);
        }

        public IActionResult MojePorudzbine()
        {
            porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
            List<Porudzbina> porudzbineTemp = new List<Porudzbina>();
            foreach (Porudzbina por in porudzbine)
            {
                if (por.IdDostavljaca==((Korisnik)Global.User).Id && por.Status == Porudzbina.EStatus.IZVRSENA)
                {
                    porudzbineTemp.Add(por);
                }

            }


            return View(porudzbineTemp);
        }

        public IActionResult PrethodnePorudzbine()
        {
            porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
            List<Porudzbina> porudzbineTemp = new List<Porudzbina>();
            foreach (Porudzbina por in porudzbine)
            {
                if (por.IdKorisnika==((Korisnik)Global.User).Id && por.Status == Porudzbina.EStatus.IZVRSENA)
                {
                    porudzbineTemp.Add(por);
                }

            }


            return View(porudzbineTemp);
        }

        public IActionResult SvePorudzbine()
        {
            porudzbine = DatabaseModel.UcitajPorudzbine(putanja);



            return View(porudzbine);
        }

        public IActionResult PrihvatiPorudzbinu(int id)
        {
            Porudzbina por = new Porudzbina();
            porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
            foreach (Porudzbina p in porudzbine)
            {
                if(p.Id == id)
                {
                    por = p;
                    por.Status = Porudzbina.EStatus.NEIZVRSENA;
                    por.IdDostavljaca = ((Korisnik)Global.User).Id;
                    DatabaseModel.UpdatePorudzbine(por, putanja);
                }
            }
            Global.Porudzbina = por;

            return RedirectToAction("Index","Shop");
        }

    }
}
