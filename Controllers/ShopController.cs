﻿using EPromet.Models;
using Microsoft.AspNetCore.Mvc;

namespace EPromet.Controllers
{
    public class ShopController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IWebHostEnvironment putanja;
        private IEnumerable<Proizvod> proizvodi;
        private double cenaDostave = 330;

        public ShopController(ILogger<HomeController> logger, IWebHostEnvironment hostEnvironment)
        {
            _logger = logger;
            putanja = hostEnvironment;
        }

        public IActionResult Index()
        {
            if (Global.User == null)
            {
                return RedirectToAction("Login", "Autentifikacija");
            }
            proizvodi = DatabaseModel.UcitajProizvode(putanja);
            return View(proizvodi);
        }

        public IActionResult Dodaj(Proizvod proizvod)
        {
            if(Global.Porudzbina == null)
            {
                Korisnik k = (Korisnik)Global.User;
                Porudzbina por = new Porudzbina();
                por.Cena = cenaDostave;// OVDE DODAj
                por.Adresa = k.Adresa;
                por.IdKorisnika = k.Id;
                por.Cena += proizvod.Cena;
                por.ListaProizvoda.Add(proizvod);
                Global.Porudzbina = por;
            }
            else
            {
                Porudzbina por = (Porudzbina)Global.Porudzbina;
                por.ListaProizvoda.Add(proizvod);
                por.Cena += proizvod.Cena;
                Global.Porudzbina = por;
            }


            return RedirectToAction("Index", "Shop");
        }

        public IActionResult Korpa()
        {
            Porudzbina p = (Porudzbina)Global.Porudzbina;

            if (p == null)
            {
                return RedirectToAction("Index", "Shop");
            }


            return View(p);

        }

        public IActionResult TrenutnaPorudzbina()
        {
            Porudzbina p = (Porudzbina)Global.Porudzbina;

            if (p == null)
            {
                return RedirectToAction("Index", "Shop");
            }


            p.Status = Porudzbina.EStatus.NA_CEKANJU;
            p.Id = DatabaseModel.SacuvajPorudzbinu(p, putanja);
            Global.Porudzbina = p;

            return RedirectToAction("MojaPorudzbina");

        }

        public IActionResult MojaPorudzbina()
        {
            Porudzbina p = (Porudzbina)Global.Porudzbina;

            if (p == null)
            {
                return RedirectToAction("Index", "Shop");
            }

            return View(p);

        }

        public IActionResult PrethodnePorudzbine()
        {
            List<Porudzbina> porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
            List<Porudzbina> tempPorudzbine = new List<Porudzbina>();

            foreach (Porudzbina p in porudzbine)
            {
                if(p.IdKorisnika==((Korisnik)Global.User).Id && p.Status.ToString() == "IZVRSENA")
                {
                    tempPorudzbine.Add(p);
                }

            }

            return View(tempPorudzbine);

        }

        public IActionResult PorudzbinaDostavljena(int id)
        {
            Porudzbina por = new Porudzbina();
            List<Porudzbina> porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
            foreach (Porudzbina p in porudzbine)
            {
                if (p.Id == id)
                {
                    por = p;
                    por.Status = Porudzbina.EStatus.IZVRSENA;
                    DatabaseModel.UpdatePorudzbine(por, putanja);
                }
            }
            Global.Porudzbina = null;

            return RedirectToAction("Index", "Shop");
        }


        public IActionResult UkloniProizvod(Proizvod p)
        {
            Porudzbina porudz = (Porudzbina)Global.Porudzbina;

            foreach (Proizvod pr in porudz.ListaProizvoda)
            {
                if(pr.Id==p.Id)
                {
                    porudz.ListaProizvoda.Remove(pr);
                    porudz.Cena -= p.Cena;
                    break;
                }
            }

            Global.Porudzbina = porudz;

            return RedirectToAction("Korpa","Shop");
        }

        public IActionResult ObrisiProizvod(Proizvod p)
        {
            DatabaseModel.ObrisiProizvod(p, putanja);


            return RedirectToAction("Index","Shop");
        }



    }
}
