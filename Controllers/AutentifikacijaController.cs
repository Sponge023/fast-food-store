﻿using EPromet.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Xml;
using System.Xml.Linq;

namespace EPromet.Controllers
{
    public class AutentifikacijaController : Controller
    {
        const string SessionName = "_Name";
        const string SessionAge = "_Age";
        List<Korisnik> korisnici = new List<Korisnik>();
        private IWebHostEnvironment putanja;

        public AutentifikacijaController(IWebHostEnvironment environment)
        {
            putanja = environment;
        }

        public IActionResult Index()
        {
            
            return View();

        }


        public IActionResult Register()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Register(Korisnik k)
        {
            DatabaseModel.SacuvajKorisnika(k, putanja);
            
            return RedirectToAction("Index", "Shop");
        }

        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(Korisnik k)
        {
            if (k.KorisnickoIme == null || k.Lozinka == null)
                return View();

            List<Korisnik> korisnici = DatabaseModel.UcitajKorisnike(putanja);

            foreach (Korisnik kor in korisnici)
            {
                if(k.KorisnickoIme == kor.KorisnickoIme)
                {
                    if(k.Lozinka == kor.Lozinka)
                    {
                        HttpContext.Session.SetString(SessionName, k.KorisnickoIme.ToString());
                        HttpContext.Session.SetInt32(SessionAge, 24);

                        Global.UserRole = kor.Uloga.ToString();
                        Global.User = kor;

                        List<Porudzbina> porudzbine = DatabaseModel.UcitajPorudzbine(putanja);
                        foreach (Porudzbina por in porudzbine)
                        {
                            if (por.IdKorisnika == kor.Id)
                            {
                                if(por.Status.ToString() == "IZVRSENA")
                                {
                                    Global.Porudzbina = null;
                                    return RedirectToAction("Index", "Shop");
                                }
                                Global.Porudzbina = por;
                                return RedirectToAction("Index", "Shop");
                            }

                            if (por.IdDostavljaca == kor.Id)
                            {
                                if (por.Status.ToString() == "IZVRSENA")
                                {
                                    Global.Porudzbina = null;
                                    return RedirectToAction("Index", "Shop");
                                }
                                Global.Porudzbina = por;
                                return RedirectToAction("Index", "Shop");
                            }
                        }

                        Global.Porudzbina = null;
                        return RedirectToAction("Index", "Shop");
                    }

                    ViewBag.ErrorMessage = "Neispravna lozinka!";
                    return View();
                }
            }

            ViewBag.ErrorMessage = "Nepostojeci racun!";

            //NEMA TAKVOG KORISNIKA


            return View();
        }

        public IActionResult Logout()
        {
            Global.UserRole = null;
            Global.User = null;
            Global.Porudzbina = null;
            return RedirectToAction("Login", "Autentifikacija");
        }
    }
}
