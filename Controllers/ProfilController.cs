﻿using EPromet.Models;
using Microsoft.AspNetCore.Mvc;

namespace EPromet.Controllers
{
    public class ProfilController : Controller
    {
        List<Korisnik> korisnici = new List<Korisnik>();
        private IWebHostEnvironment putanja;

        public ProfilController(IWebHostEnvironment environment)
        {
            putanja = environment;
        }


        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Details()
        {
            Korisnik k = (Korisnik)Global.User;

            return View(k);
        }

        public IActionResult Edit()
        {
            Korisnik k = (Korisnik)Global.User;

            return View(k);
        }

        [HttpPost]
        public IActionResult Edit(Korisnik k)
        {
            string filePath = "Database/Images/";
            filePath += $"{k.Id}-{k.KorisnickoIme}.png";

            k.AvatarPath = filePath;
            DatabaseModel.UpdateKorisnika(k, putanja);
            Global.User = k;

            return RedirectToAction("Details");
        }

        public IActionResult Verifikacija()
        {
            List<Korisnik> korisnici = DatabaseModel.UcitajKorisnike(putanja);


            return View(korisnici);
        }

        public IActionResult VerifikacijaKorisnika(int id)
        {
            List<Korisnik> korisnici = DatabaseModel.UcitajKorisnike(putanja);
            foreach (Korisnik kor in korisnici)
            {
                if(kor.Id == id)
                {
                    kor.Verifikovan = true;
                    kor.Uloga = Korisnik.EUloga.DOSTAVLJAC;
                    DatabaseModel.UpdateKorisnika(kor, putanja);
                }
            }


            return RedirectToAction("Verifikacija","Profil");
        }

        public IActionResult OdobrenaRegistracija(int id)
        {
            List<Korisnik> korisnici = DatabaseModel.UcitajKorisnike(putanja);
            foreach (Korisnik kor in korisnici)
            {
                if(kor.Id == id)
                {
                    kor.Uloga = Korisnik.EUloga.KORISNIK;
                    DatabaseModel.UpdateKorisnika(kor, putanja);
                }
            }


            return RedirectToAction("Verifikacija","Profil");
        }

        public IActionResult UnverifikacijaKorisnika(int id)
        {
            List<Korisnik> korisnici = DatabaseModel.UcitajKorisnike(putanja);
            foreach (Korisnik kor in korisnici)
            {
                if(kor.Id == id)
                {
                    kor.Verifikovan = false;
                    kor.Uloga = Korisnik.EUloga.KORISNIK;
                    DatabaseModel.UpdateKorisnika(kor, putanja);
                }
            }


            return RedirectToAction("Verifikacija","Profil");
        }
    }
}
