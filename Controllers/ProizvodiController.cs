﻿using EPromet.Models;
using Microsoft.AspNetCore.Mvc;

namespace EPromet.Controllers
{
    public class ProizvodiController : Controller
    {

        private IWebHostEnvironment putanja;


        public ProizvodiController(IWebHostEnvironment environment)
        {
            putanja = environment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Proizvod p)
        {
            DatabaseModel.SacuvajProizvod(p, putanja);

            return RedirectToAction("Index","Shop");
        }
        
    }
}
